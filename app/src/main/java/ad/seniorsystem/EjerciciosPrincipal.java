package ad.seniorsystem;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class EjerciciosPrincipal extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicios_principal);



        ImageButton btnArtritis = (ImageButton) findViewById(R.id.imageArtritis);
        btnArtritis.setOnClickListener(this);

        ImageButton btnArtrosis = (ImageButton) findViewById(R.id.imageArtrosis);
        btnArtrosis.setOnClickListener(this);

        ImageButton btnEsclerosis = (ImageButton) findViewById(R.id.imageEsclerosis);
        btnEsclerosis.setOnClickListener(this);

        ImageButton btnParkinson = (ImageButton) findViewById(R.id.imageParkinson);
        btnParkinson.setOnClickListener(this);
    }

    public void lanzaActividad(int idView) {

        Intent i;
        switch (idView) {

            case R.id.imageArtritis:
                i = new Intent(this, EjerciciosArtritisActivity.class);
                startActivity(i);
                break;
            case R.id.imageArtrosis:
                i = new Intent(this, EjerciciosArtrosisActivity.class);
                startActivity(i);
                break;
            case R.id.imageEsclerosis:
                i = new Intent(this, EjerciciosEsclerosisActivity.class);
                startActivity(i);
                break;

            case R.id.imageParkinson:
                i = new Intent(this, EjerciciosParkinsonActivity.class);
                startActivity(i);
                break;


        }



    }


    @Override
    public void onClick(View view) {
        lanzaActividad(view.getId());
    }


}
