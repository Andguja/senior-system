package ad.seniorsystem;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng casa1 = new LatLng(6.2084834, -75.5688086);
        mMap.addMarker(new MarkerOptions().position(casa1).title("Casa de Andres Agudelo (Nieto)"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(casa1));
        CameraPosition cameraPosition = CameraPosition.builder().target(casa1).zoom(14).build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        LatLng casa2 = new LatLng(6.250155, -75.619813);
        mMap.addMarker(new MarkerOptions().position(casa2).title("Casa de Denny Villalobos (Hijo)"));
        LatLng casa3 = new LatLng(6.250489, -75.609962);
        mMap.addMarker(new MarkerOptions().position(casa3).title("Casa de Camila Martinez (Hermana)"));
        LatLng casa4 = new LatLng(6.243919, -75.602945);
        mMap.addMarker(new MarkerOptions().position(casa4).title("Casa de Daniel Jaramillo (Nieto)"));
        LatLng casa5 = new LatLng(6.238267, -75.606206);
        mMap.addMarker(new MarkerOptions().position(casa5).title("Casa de Alejandra Agudelo (Nieto)"));
        LatLng casa6 = new LatLng(6.203155, -75.570801);
        mMap.addMarker(new MarkerOptions().position(casa6).title("Casa de Camil Villalobos (Nieto)"));
        LatLng casa7 = new LatLng(6.195348, -75.565158);
        mMap.addMarker(new MarkerOptions().position(casa7).title("Casa de Andres Martinez (Nieto)"));
        LatLng casa8 = new LatLng(6.180543, -75.567604);
        mMap.addMarker(new MarkerOptions().position(casa8).title("Casa de Edilberto Agudelo (Nieto)"));
        LatLng casa9 = new LatLng(6.241108 , -75.585388);
        mMap.addMarker(new MarkerOptions().position(casa9).title("Mi casa"));
    }
}
