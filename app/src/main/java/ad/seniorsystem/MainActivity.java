package ad.seniorsystem;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        ImageButton btnEjercicios = (ImageButton) findViewById(R.id.imageEjercicios);
        btnEjercicios.setOnClickListener(this);

        ImageButton btnAgenda = (ImageButton) findViewById(R.id.imageCalendario);
        btnAgenda.setOnClickListener(this);

        ImageButton btnMapa = (ImageButton) findViewById(R.id.imageMapa);
        btnMapa.setOnClickListener(this);

        ImageButton btnEmergencia = (ImageButton) findViewById(R.id.imageEmergencia);
        btnEmergencia.setOnClickListener(this);
    }

    public void lanzaActividad(int idView) {

        Intent i;
        switch (idView) {

            case R.id.imageEjercicios:
                i = new Intent(this, EjerciciosPrincipal.class);
                startActivity(i);
                break;
            case R.id.imageCalendario:
                i = new Intent(this, CalendarioActivity.class);
                startActivity(i);
                break;
            case R.id.imageMapa:
                i = new Intent(this, MapsActivity.class);
                startActivity(i);
                break;

            case R.id.imageEmergencia:
                i = new Intent(Intent.ACTION_CALL);
                i.setData(Uri.parse("tel:3136118133"));
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                startActivity(i);

        }



    }


    @Override
    public void onClick(View view) {


        lanzaActividad(view.getId());
    }


}
